import pytest

from .address_formatting import address_formatting


@pytest.mark.parametrize(
    "input_string, expected_result",
    (
        ("Winterallee 3", {"street": "Winterallee", "housenumber": "3"}),
        ("Musterstrasse 45", {"street": "Musterstrasse", "housenumber": "45"}),
        ("Blaufeldweg 123B", {"street": "Blaufeldweg", "housenumber": "123B"}),
        ("Blaufeldweg", {"street": "Blaufeldweg", "housenumber": ""}),
    ),
)
def test_address_formatting_level_1(input_string, expected_result):
    result = address_formatting(input_string)
    assert result == expected_result


@pytest.mark.parametrize(
    "input_string, expected_result",
    (
        ("Am Bächle 23", {"street": "Am Bächle", "housenumber": "23"}),
        (
            "Auf der Vogelwiese 23 b",
            {"street": "Auf der Vogelwiese", "housenumber": "23 b"},
        ),
    ),
)
def test_address_formatting_level_2(input_string, expected_result):
    result = address_formatting(input_string)
    assert result == expected_result


@pytest.mark.parametrize(
    "input_string, expected_result",
    (
        (
            "4, rue de la revolution",
            {"street": "rue de la revolution", "housenumber": "4"},
        ),
        ("200 Broadway Av", {"street": "Broadway Av", "housenumber": "200"}),
        ("Calle Aduana, 29", {"street": "Calle Aduana", "housenumber": "29"}),
        ("Calle 39 No 1540", {"street": "Calle 39", "housenumber": "1540"}),
        (
            "SQN 408 BL B 101",
            {"street": "SQN 408 BL B", "housenumber": "101"},
        ),  # Extra Brazilian address
    ),
)
def test_address_formatting_level_3(input_string, expected_result):
    result = address_formatting(input_string)
    assert result == expected_result
