import re


def address_formatting(string):
    rgx = r"(\d+\s*(?:[a-zA-Z]\s?$)?)"
    digits = re.findall(rgx, string, re.IGNORECASE)
    street = string
    housenumber = ""
    if digits:
        housenumber = digits[-1].strip()
        street = string.replace(housenumber, "")
        street = re.sub(r"\W", " ", street)
        street = re.sub(r"\s(?:No|Num|Number|N)\s", "", street)
        housenumber = re.sub(r"[\,|\.]", "", housenumber)
    return {"street": street.strip(), "housenumber": housenumber}
