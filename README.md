# Address Formatting

Formatting address strings into easy to integrate json


Installing
----------

### Locally

Running the project locally is also very simple:

- Install requirements using pip:

.. code:: bash

    pip install -r requirements.txt

- Or poetry (https://python-poetry.org/)

.. code:: bash

    poetry install
    poetry shell #Starts virtual environment

- Run python and import the address_formatting tool:

.. code:: python

    from tools.address_formatting import address_formatting
    
    string = "Winterallee 3"
    address_formatting(string)
    #  {"street": "Winterallee", "housenumber": "3"} 


Tests
-------
#### Locally
.. code:: bash

    make test
